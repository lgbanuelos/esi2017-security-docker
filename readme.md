# Role-based access for Angular/Spring boot

In this repository, you will find a fully-fledged project (backend+backend) that illustrates how to add role-based access to our applications. I have committed the code in a way that will help you understand all the components. Please try to read the message associated to each commit and you would be able to understand the purpose of the code that was added.

The goal of the code is to handle authentication and then, based on the user's role, allow/deny access to one of the two resources. USER1 has access to RESOURCE1, USER2 has access to RESOURCE2 and ADMIN has access to both.

# Docker

In the last commit, you will find the files that are required to bundle the application as two containers. Please follow the steps below:

## Package the backend

We will first create a "fat-jar" containing the full backend application. To that end, in your IntelliJ IDE, press Ctrl+A, enter "execute maven" and then press Return. This will open a dialog window where you will be able to select a maven goal. This time you will have to enter "package" and then press Return. At the end of the task, you will have a jar file within the folder "target", with the name "demo-0.0.1-SNAPSHOT.jar". NOTE: You have to repeat this operation every time you change your backend application.

## Create a docker image containing your backend application

Go to the backend's root folder. There you will see the corresponding `Dockerfile`. I have explained its content during the lecture (please have a look at the corresponding video). Use the following command to create your image:

```
docker build -t lgbanuelos/backend .
```

NOTE: Change 'lgbanuelos' to your own docker username. Remember also to change that name in the file `docker-compose.yml` that will be referred later.

## Compile your Angular application for packaging

Similar to the backend, we have to create the files of your frontend application for deployment. To that end, please change to the folder containing the frontend application. There, run the following command:

```
ng build
```

NOTE: I am assuming that you have completed the code and also imported the node packages (i.e. npm import).

The above will compile the typescript files and produce a set of javascript files that are ready for deployment. That is all you need.

## Create an image containing your frontend application

I have also included a `Dockerfile` for your frontend application. In the frontend application, you just need to run the following command:

```
docker build -t lgbanuelos/frontend .
```

## Run the applications

To run the backend, you will need to use the following command:

```
docker run -it -p 8080:8080 lgbanuelos/backend
```

Similarly, to run the frontend, you have to use the following command:

```
docker run -it -p 8090:80 lgbanuelos/frontend
```

Note that the backend port will be accessible via the port 8080 in the host operating system. Similarly, the frontend will be accessible via the port 8090. Therefore, you can check your application using a browser and going to the URL http://localhost:8090

## Using docker-compose

Docker compose is a tool that allow us to instantiate composite applications by assembling a set of containers and connecting them using a virtual network. You will find the file `docker-compose.yml` in the root folder of the repository. To run the application, you will need first to stop the applications above (use Control-C). Then, go to the root folder of the repository and run the command:

```
docker-compose up
```

NOTE: Do not forget to change the username in the file `docker-composer.yml` above (I am assuming that you changed 'lgbanuelos' to your own username in the steps above)

Again, you will be able to check the application in your browser using the URL http://localhost:8090
