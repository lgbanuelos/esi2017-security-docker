import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AuthenticationComponent } from "app/authentication/authentication.component";
import { AuthenticationService } from "app/authentication/authetication.service";
import { WelcomeComponent } from "app/welcome/welcome.component";
import { Resource1Component } from "app/resource1/Resource1.component";
import { Resource2Component } from "app/resource2/resource2.component";

import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    WelcomeComponent,
    Resource1Component,
    Resource2Component,
    AuthenticationComponent,
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', component: WelcomeComponent },
      { path: 'login', component: AuthenticationComponent },
      { path: 'resource1', component: Resource1Component },
      { path: 'resource2', component: Resource2Component }
    ])
  ],
  providers: [AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
