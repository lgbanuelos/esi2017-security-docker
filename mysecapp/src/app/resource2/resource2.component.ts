import { Component, OnInit } from "@angular/core";
import { Http, RequestOptions } from "@angular/http";
import { Router } from "@angular/router";
import { AuthenticationService } from "app/authentication/authetication.service";
import { environment } from "environments/environment";

@Component({
  selector: 'resource2',
  template: `Resource2: {{msg}}`
})
export class Resource2Component implements OnInit {
    msg: String = '';
    constructor(private http: Http, 
        private authenticationService: AuthenticationService,
        private router: Router) {
    }
    ngOnInit() {
        if (!this.authenticationService.checkRoles(['ROLE_USER2', 'ROLE_ADMIN']))
            this.router.navigateByUrl('');
        else
            this.http.get(environment.BASE_URL + "/api/service/resource2", new RequestOptions({headers: this.authenticationService.headers()}))
                .subscribe(res => this.msg = res.json().msg);
    }
}