import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { RequestOptions, Headers, Http } from "@angular/http";
import { environment } from "environments/environment";

import "rxjs/add/operator/map";

@Injectable()
export class AuthenticationService {
    constructor(private http:Http) {}
    roles: string[] = [];
    authenticate(username: string, password: string)
        : Observable<boolean> {
        localStorage.setItem("code", btoa(username +":"+password));
        var options = new RequestOptions({headers: this.headers()});
        return this.http.get(environment.BASE_URL + "/api/authenticate", options)
            .map( res => { this.roles = res.json(); console.log(res.json()); return true;})
    }

    headers() {
        let value = new Headers();
        let code = localStorage.getItem("code");
        if (!!code)
            value.set("Authorization", `Basic ${code}`);
        return value;
    }

    // This method checks if the roles associated to a user overlaps a
    // specific set of roles
    checkRoles(roles: String[]) {
        if (this.roles.length > 0)
            return this.roles.filter( n => roles.indexOf(n) >= 0).length > 0;
        return false;
    }

    logout() {
        this.roles = [];
        localStorage.removeItem("code");
    }
}