import { Component } from "@angular/core";
import { AuthenticationService } from "app/authentication/authetication.service";
import { Router } from "@angular/router";

@Component({
    selector: "login",
    template: `
    <input type="text" [(ngModel)]="username">
    <input type="password" [(ngModel)]="password">
    <button (click)="login()">Login</button>
    `
})
export class AuthenticationComponent {
    username: string;
    password: string;
    constructor(private authentication: AuthenticationService, private router: Router){}
    login() {
        this.authentication.authenticate(this.username, this.password)
            .subscribe(res => this.router.navigateByUrl(''));
    }
}