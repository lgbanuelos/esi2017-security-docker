import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "app/authentication/authetication.service";

@Component({
  selector: 'app-root',
  template: `
      <a routerLink="/login">Login</a>
      <a routerLink="/resource1">Resource1</a>
      <a routerLink="/resource2">Resource2</a>
      <a href="javascript:void(0)" (click)="logout()">Logout</a>
      <div>
        <div>
          <router-outlet></router-outlet>
        </div>
      </div>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  constructor(private router: Router, private authenticationService: AuthenticationService){}

  logout() {
    this.authenticationService.logout();
    this.router.navigateByUrl('');
  }
}
