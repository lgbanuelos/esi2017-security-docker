import { Component } from "@angular/core";

@Component({
  selector: 'welcome',
  template: 'You are in the welcome page'
})
export class WelcomeComponent {
}