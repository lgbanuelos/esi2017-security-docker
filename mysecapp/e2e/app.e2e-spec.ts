import { MysecappPage } from './app.po';

describe('mysecapp App', () => {
  let page: MysecappPage;

  beforeEach(() => {
    page = new MysecappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
